
(deffunction ask-question (?question)
	(printout t ?question crlf)
	(readline)
)

(deffunction ask-choice-question (?question $?allowed-values)
   (printout t crlf)
   (printout t "Allowed values are: " $?allowed-values)
   (bind ?answer (ask-question ?question))	
   (while (eq (member$ ?answer ?allowed-values) FALSE) do
   	  (printout t "Bad answer: " ?answer  " Allowed values are: " ?allowed-values crlf)
      (bind ?answer (ask-question ?question))
	  )
   ?answer)

(defrule find-related
	(declare (salience 160))
	(book ?recommendation)
	(user-genre ?user-genre-input)
	=>
	(printout t "See related to " (send (instance-name ?recommendation) get-title) "?")
	(bind ?answer (ask-choice-question "" "ok" "no"))
	(if (= (str-compare ?answer ok) 0)
		then
		(bind $?genres (send (instance-name ?recommendation) get-genre))
		(bind ?count (length$ $?genres))
		(bind ?i 1)
		(loop-for-count ?count do
			(if (neq (str-compare ?user-genre-input (nth$ ?i $?genres)) 0)
				then 
				(assert (user-genre (nth$ ?i $?genres)))
				  (break )
			)
		)

		(printout t "**" $?genres crlf)
	)
)

(defrule find-reviews
	(declare (salience 170))
	(book ?recommendation)
	(object (is-a Review) (title ?title) (reviewed ?reviewed) (opinion ?opinion) (author ?author))
	(test (eq (instance-address ?reviewed) (instance-address ?recommendation)))
	=>
	(printout t "-- See also " ?opinion " review for " (send (instance-name ?reviewed) get-title) " : " ?title " by " (send (instance-name ?author) get-name_) crlf)
)

(defrule print-recommendations 
	(declare (salience 180))
	(book ?recommendation)
	=>
	(printout t "- I recommend " (send (instance-name ?recommendation) get-title) crlf)
)

(defrule propose-genre-book
	(declare (salience 190))
	(user-genre ?user-genre-input)
	?book <- (object (is-a Book) (genre $?genres) (title ?title))
	(test (member$ ?user-genre-input ?genres))
	=>
	(printout t "What about " ?title "?")
	(bind ?answer (ask-choice-question "" "ok" "no"))
	(if (= (str-compare ?answer ok) 0)
		then
		 (assert (book ?book))
	)
     
)

(defrule input
(declare (salience 200))
(initial-fact)
=>
(assert (all-instances))
(bind ?desired-genre(ask-question "What genre do you feel like reading ?"))
(assert (user-genre ?desired-genre)) 
)
