; Thu Jan 28 23:56:31 EET 2016
; 
;+ (version "3.5")
;+ (build "Build 663")


(defclass %3ACLIPS_TOP_LEVEL_SLOT_CLASS "Fake class to save top-level slot information"
	(is-a USER)
	(role abstract)
	(multislot influences
		(type STRING)
		(create-accessor read-write))
	(single-slot name_
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot title
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(multislot author
		(type INSTANCE)
;+		(allowed-classes Author)
		(cardinality 1 ?VARIABLE)
		(create-accessor read-write))
	(single-slot age
		(type INTEGER)
		(range 0 130)
;+		(cardinality 0 1)
		(create-accessor read-write))
	(single-slot occupation
		(type STRING)
;+		(cardinality 0 1)
		(create-accessor read-write))
	(single-slot opinion
		(type SYMBOL)
		(allowed-values Positive Mixed Negative)
		(default Mixed)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot date_born
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot reviewed
		(type INSTANCE)
;+		(allowed-classes Literary_Work)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot language
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(multislot books_read
		(type INSTANCE)
;+		(allowed-classes Book)
		(create-accessor read-write))
	(multislot works
		(type INSTANCE)
;+		(allowed-classes Literary_Work)
		(create-accessor read-write))
	(single-slot gender
		(type SYMBOL)
		(allowed-values male female)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(multislot nationality
		(type STRING)
		(create-accessor read-write))
	(multislot genre
		(type STRING)
		(create-accessor read-write)))

(defclass Person
	(is-a USER)
	(role concrete)
	(pattern-match reactive)
	(single-slot name_
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot age
		(type INTEGER)
		(range 0 130)
;+		(cardinality 0 1)
		(create-accessor read-write))
	(single-slot occupation
		(type STRING)
;+		(cardinality 0 1)
		(create-accessor read-write))
	(single-slot gender
		(type SYMBOL)
		(allowed-values male female)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(multislot nationality
		(type STRING)
		(create-accessor read-write)))

(defclass Author
	(is-a Person)
	(role concrete)
	(pattern-match reactive)
	(single-slot language
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(multislot genre
		(type STRING)
		(create-accessor read-write))
	(multislot works
		(type INSTANCE)
;+		(allowed-classes Literary_Work)
		(create-accessor read-write)))

(defclass Writer
	(is-a Author)
	(role concrete)
	(pattern-match reactive)
	(multislot influences
		(type STRING)
		(create-accessor read-write))
	(multislot works
		(type INSTANCE)
;+		(allowed-classes Book)
		(create-accessor read-write))
	(single-slot occupation
		(type STRING)
;+		(value "Writer")
;+		(cardinality 0 1)
		(create-accessor read-write)))

(defclass Critic
	(is-a Author)
	(role concrete)
	(pattern-match reactive)
	(single-slot genre
		(type STRING)
;+		(value "Critic")
;+		(cardinality 0 1)
		(create-accessor read-write))
	(multislot works
		(type INSTANCE)
;+		(allowed-classes Review)
		(create-accessor read-write))
	(single-slot occupation
		(type STRING)
;+		(value "Critic")
;+		(cardinality 0 1)
		(create-accessor read-write)))

(defclass Reader
	(is-a Person)
	(role concrete)
	(pattern-match reactive)
	(multislot books_read
		(type INSTANCE)
;+		(allowed-classes Book)
		(create-accessor read-write))
	(multislot genre
		(type STRING)
		(create-accessor read-write)))

(defclass Literary_Work
	(is-a USER)
	(role concrete)
	(pattern-match reactive)
	(single-slot language
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot title
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(multislot author
		(type INSTANCE)
;+		(allowed-classes Author)
		(cardinality 1 ?VARIABLE)
		(create-accessor read-write)))

(defclass Book
	(is-a Literary_Work)
	(role concrete)
	(pattern-match reactive)
	(multislot genre
		(type STRING)
		(create-accessor read-write))
	(multislot author
		(type INSTANCE)
;+		(allowed-classes Writer)
		(cardinality 1 ?VARIABLE)
		(create-accessor read-write)))

(defclass Review
	(is-a Literary_Work)
	(role concrete)
	(pattern-match reactive)
	(single-slot opinion
		(type SYMBOL)
		(allowed-values Positive Mixed Negative)
		(default Mixed)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(multislot author
		(type INSTANCE)
;+		(allowed-classes Critic)
		(cardinality 1 ?VARIABLE)
		(create-accessor read-write))
	(single-slot reviewed
		(type INSTANCE)
;+		(allowed-classes Literary_Work)
;+		(cardinality 1 1)
		(create-accessor read-write)))
