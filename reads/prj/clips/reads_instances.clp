; Thu Jan 28 23:56:31 EET 2016
; 
;+ (version "3.5")
;+ (build "Build 663")

(definstances fn71482
([KB_870893_Class26] of  Reader

	(age 31)
	(books_read [KB_870893_Class32])
	(gender male)
	(genre
		"Fiction"
		"Fantasy"
		"Thriller")
	(name_ "Peter Ivanov")
	(nationality "Bulgarian")
	(occupation "Accountant"))

([KB_870893_Class27] of  Writer

	(gender male)
	(genre
		" Fantasy"
		"Fiction"
		"Children's ")
	(influences
		"Catholicism"
		"Norse Mythology"
		"Anglo-Saxon poetry"
		" Finnish Mythology")
	(language "English")
	(name_ "J.R.R. Tolkien")
	(nationality "English")
	(works
		[KB_870893_Class32]
		[KB_870893_Class30]))

([KB_870893_Class28] of  Critic

	(gender male)
	(language "English")
	(name_ "W. H. Auden")
	(nationality "English")
	(works
		[KB_870893_Class33]
		[KB_870893_Class29]))

([KB_870893_Class29] of  Review

	(author [KB_870893_Class28])
	(language "English")
	(opinion Positive)
	(reviewed [KB_870893_Class30])
	(title "At the End of the Quest, Victory"))

([KB_870893_Class30] of  Book

	(author [KB_870893_Class27])
	(genre
		"Fiction"
		"Adventure"
		"Fantasy")
	(language "English")
	(title "The Return of the King"))

([KB_870893_Class32] of  Book

	(author [KB_870893_Class27])
	(genre
		"Fantasy"
		"Fiction"
		"Adventure")
	(language "Enlish")
	(title "The Fellowship of the Ring"))

([KB_870893_Class33] of  Review

	(author [KB_870893_Class28])
	(language "English")
	(opinion Positive)
	(reviewed [KB_870893_Class32])
	(title "The Hero Is a Hobbit"))

([KB_870893_Class34] of  Writer

	(age 51)
	(gender female)
	(genre
		"Fantasy"
		"Fiction"
		"Adventure"
		"Young adults")
	(influences
		"Oscar Wilde"
		"C.S Lewis"
		"Jane Austin")
	(language "English")
	(name_ "J.K. Rowling")
	(nationality "Enlish")
	(works [KB_870893_Class35]))

([KB_870893_Class35] of  Book

	(author [KB_870893_Class34])
	(genre
		"Fiction"
		"Fantasy"
		"Young Adult")
	(language "English")
	(title "Harry Potter and the Sorcerer's Stone"))

([KB_870893_Class36] of  Critic

	(age 90)
	(gender female)
	(language "English")
	(name_ "Alison Lurie")
	(nationality "American ")
	(works [KB_870893_Class37]))

([KB_870893_Class37] of  Review

	(author [KB_870893_Class36])
	(language "English")
	(opinion Positive)
	(reviewed [KB_870893_Class35])
	(title "Not for Muggles"))

([KB_870893_Class38] of  Book

	(author [KB_870893_Class39])
	(genre
		"Fiction"
		"Romance"
		"Mysticism")
	(language "Russian")
	(title "The Master and Margarita"))

([KB_870893_Class39] of  Writer

	(gender male)
	(genre
		"Fantasy"
		"Satire"
		"Short Stories")
	(influences
		" Anton Chekov"
		"Fyodor Dostoyevsky")
	(language "Russian")
	(name_ "Mikhail Bulgakov")
	(nationality "Soviet Union")
	(works [KB_870893_Class38]))

([KB_870893_Class40] of  Review

	(author [KB_870893_Class41])
	(language "English")
	(opinion Mixed)
	(reviewed [KB_870893_Class38])
	(title "Sympathy for the Devil"))

([KB_870893_Class41] of  Critic

	(age 92)
	(gender male)
	(language "English")
	(name_ "Michael Wood")
	(nationality "English")
	(works [KB_870893_Class40]))

([KB_870893_Class42] of  Reader

	(age 16)
	(books_read [KB_870893_Class35])
	(gender female)
	(genre
		"Romance"
		"Young Adult"
		"Fiction")
	(name_ "Jessica Sims")
	(nationality "English")))
