#!/bin/sh

echo -e "How big do you want the map to be?\n0<width<52 0<height"
read width height
echo "width is $width height is $height"

mkdir -p tmp

# Generate default map
java -jar bin/mapgen.jar -w $width -h $height -l -f tmp/map
STATUS=$?

if [ $STATUS -ne 0 ]
	then
		exit 1
fi


cat tmp/map.csv

echo -e "\nChoose start and end points within the map.\n [A-Za-z][>1] [A-Za-z][>1]"
read startPoint endPoint
echo "startPoint is $startPoint endPoint is $endPoint"

java -jar bin/astar.jar tmp/map.csv $startPoint $endPoint tmp/route
STATUS=$?


while [ $STATUS -eq 0 ]; do
    read -p "Do you want to see the route marked on the map?" yn
    case $yn in
        [Yy]* )
         java -jar bin/mazetrace.jar tmp/map.csv tmp/route tmp/routed
		 cat tmp/routed.csv; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

