Tools for generating mazes and solving them using A*

Requirments
-Java 8

TL;DR - run wizard.sh

# mapgen
	Usage:
	Generates mazes according to the provided params.

	Params:
	"--width", aliases = "-w", usage = "Width (horizontal size) of the map", required = true

    "--height", aliases = "-h", usage = "Height (vertical size) of the map", required = true

    "--walls-count", aliases = "-wc", usage = "Number of walls to use in the map.
    Must comply with Walls+Water < width*height. Default is 12.5%(w*h-wtc)"

    "--water-count", aliases = "-wtc", usage = "Number of water fields to use in the map.
    Must comply with Walls+Water < width*height. Default is 12.5%(w*h-wc)"

    "--file-name", aliases = "-f", usage = "Output filename. Default is \"test\""

    "--labels", aliases = "-l", usage = "Add labels to columns and rows"

    Syntax:
    [--file-name (-f) VAL] --height (-h) N [--labels (-l)] [--walls-count (-wc) N] [--water-count (-wtc) N] --width (-w)

    Example:
    java -jar mapgen.jar -w 15 -h 10 -l -f someMap 

    Note:
    Currently only generates horizontal labels up to 52 - A-Za-z
# astar
	Usage:
	Applies A* to the provided maze with the provided start and end points. In order to use with 
	mapgen the "-l" switch should be included when running mapgen.

	Params:
	Space separated params
	[0] File path to the maze that should be loaded, required = true
	[1] Start point for the algorithm, required = true
	[2] End point for the algorithm, required = true
	[3] File path for output of the generated route

	Syntax:
	mazeFileName.csv [A-Za-z]1-h [A-Za-z]1-h [routeFileName]

	Example:
	java -jar astar.jar someMap.csv A1 D4 path.txt

# mazetrace
	Usage:
	Creates copy of the provided maze with marked fields according to the given route.

	Params:
	Space separated params
	[0] File path to the maze that should be loaded, required = true
	[1] File path to the route instructions, required = true
	[2] File path for the resulting maze with the applied route, required = true

	Syntax:
	mazeFileName.csv routeFileName routedMazeFileName

	Example:
	java -jar mazetrace.jar someMap.csv path.txt routedMap

All binaries are in bin/
