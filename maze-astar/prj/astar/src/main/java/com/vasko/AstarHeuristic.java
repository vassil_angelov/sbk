package com.vasko;

@FunctionalInterface
public interface AstarHeuristic<T> {

    double estimate(T from, T to);
}
