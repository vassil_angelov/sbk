package com.vasko;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        if (args.length != 3 && args.length != 4) {
            System.out.println("Incorrect number of arguments. Needed 3 or 4, but got " + args.length);
            System.exit(-1);
        }

        Maze maze = new Maze();
        if (!maze.parse(args[0])) {
            System.out.println("Invalid file for maze - " + args[0]);
            System.exit(-1);
        }

        String startingPointArg = args[1];
        Maze.Cell startingPoint = maze.cellAt(startingPointArg.charAt(0), Integer.parseInt(startingPointArg.substring(1)));

        if (startingPoint == null) {
            System.out.println("Starting point not in the maze");
            System.exit(-1);
        }

//        System.out.println("startPoint=" + startingPoint.toString());
//        System.out.println("startPoint=" + startingPoint.getX() + " " + startingPoint.getY());

        String endPointArg = args[2];
        Maze.Cell endPoint = maze.cellAt(endPointArg.charAt(0), Integer.parseInt(endPointArg.substring(1)));

        if (endPoint == null) {
            System.out.println("End point not in the maze");
            System.exit(-1);
        }

//        System.out.println("endPoint" + endPoint.toString());
//        System.out.println("endPoint=" + endPoint.getX() + " " + endPoint.getY());
        if (startingPoint.getSign() != 'N' && endPoint.getSign() != 'N') {
            AstarMaze astar = new AstarMaze((from, to) -> {
                // heuristic is the distance between the cells in 2d field
//                System.out.println("Heuristic for" + from.toString() + "=" + Math.sqrt(Math.pow(from.getX() - to.getX(), 2) + Math.pow(from.getY() - to.getY(), 2)));
                return Math.sqrt(Math.pow(from.getX() - to.getX(), 2) + Math.pow(from.getY() - to.getY(), 2));

            });

            List<Maze.Cell> path = astar.apply(maze, startingPoint, endPoint);
            if (path == null) {
                System.out.println("Not path found between " + startingPoint.toString() + " and " + endPoint.toString());
            } else {
                String pathString = Arrays.deepToString(path.toArray());
                System.out.println(pathString);
                if (args.length == 4) {
                    try {
                        FileWriter fileWriter = new FileWriter(args[3]);
                        fileWriter.append(pathString);
                        fileWriter.flush();
                        fileWriter.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                        System.out.println("Unable to write path to " + args[3]);
                        System.exit(-1);
                    }
                }
            }
        } else {
            System.out.println("Starting point and end point must be blank fields");
            System.exit(-1);
        }
    }
}
