package com.vasko;

import java.util.*;

public class AstarMaze {

    private class Node implements Comparable<Node> {
        Node parent;
        Maze.Cell data;
        double g, h;

        public Node(Node parent, Maze.Cell data, double g, double h) {
            this.parent = parent;
            this.data = data;
            this.g = g;
            this.h = h;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Node node = (Node) o;

            return data.equals(node.data);

        }

        @Override
        public int hashCode() {
            return data.hashCode();
        }

        @Override
        public String toString() {
            return "Node{" +
                    "data=" + data + "," +
                    "g+h=" + (g + h) +
                    '}';
        }

        @Override
        public int compareTo(Node o) {
//            System.out.println("Compare " + data.toString() + " and " + o.data.toString() + "=" + (int) ((h + g) - (o.g + o.h)));
            return (int) (((h + g) - (o.h + o.g)) * 10);
        }
    }

    private AstarHeuristic<Maze.Cell> heuristic;

    public AstarMaze(AstarHeuristic<Maze.Cell> heuristic) {
        this.heuristic = heuristic;
    }

    public List<Maze.Cell> apply(Maze maze, Maze.Cell startPoint, Maze.Cell endPoint) {
        PriorityQueue<Node> open = new PriorityQueue<>();
        Set<Maze.Cell> closed = new HashSet<>();
        open.add(wrapCell(startPoint, null, endPoint));

        while (!open.isEmpty()) {
            Node current = open.poll();
//            System.out.println("Open queue head = " + current.data.toString());
            List<Maze.Cell> successors = maze.availableNeighbours(current.data);
//            System.out.println("Neighbours for" + current.data.toString() + " : " + Arrays.deepToString(successors.toArray()));
            for (Maze.Cell successor : successors) {
                Node wrapped = wrapCell(successor, current, endPoint);
                if (successor.equals(endPoint)) {
                    return unwrapParents(wrapped);
                }

                if (closed.contains(successor)) {
                    continue;
                }

                boolean shouldAdd = !open.contains(wrapped) || open.removeIf(node -> wrapped.equals(node) && wrapped.compareTo(node) < 0);

                if (shouldAdd) {
                    open.offer(wrapped);
                }

            }
            closed.add(current.data);
        }

        return null;
    }

    private List<Maze.Cell> unwrapParents(Node node) {
        List<Maze.Cell> parents = new ArrayList<>();
        parents.add(node.data);
        while (node.parent != null) {
            parents.add(node.parent.data);
            node = node.parent;
        }

        Collections.reverse(parents);
        return parents;
    }

    private Node wrapCell(Maze.Cell cell, Node parent, Maze.Cell endPoint) {
        double g = parent != null ? parent.g + cell.getCost() : cell.getCost();
        double h = heuristic.estimate(cell, endPoint);
        return new Node(parent, cell, g, h);
    }
}
