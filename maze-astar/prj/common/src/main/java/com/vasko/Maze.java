package com.vasko;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Maze {

    public class Cell {
        private int x;
        private int y;
        private char sign;
        private double cost;

        private Cell(int x, int y, char sign) {
            this.x = x;
            this.y = y;
            this.sign = sign;
        }

        public char getSign() {
            return sign;
        }

        public double getCost() {
            return cost;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Cell cell = (Cell) o;
            return Objects.equals(x, cell.x) &&
                    Objects.equals(y, cell.y);
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }

        @Override
        public String toString() {
            String labelX = "";
            if (x < 52) {
                if (x < 26) {
                    labelX = Character.toString((char) ('A' + x));
                } else {
                    labelX = Character.toString((char) (('a' + x) - 26));
                }
            } else {
                labelX = "#" + x;
            }
            return "(" + labelX + "," + (y + 1) + ")";
        }
    }

    private char[][] map;
    private List<String> rawMapList;

    public Maze() {
        rawMapList = new ArrayList<>();
    }

    public boolean parse(String file) {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                if (!sCurrentLine.isEmpty()) {
                    //trim front
                    sCurrentLine = sCurrentLine.replaceAll("^\\s+", "");
                    rawMapList.add(sCurrentLine);
                }
            }
            initMap();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean save(String fileName) {
        CsvFile csv = new CsvFile(fileName);

        for (String label : rawMapList.get(0).split(",")) {
            if (label.equals("#")) {
                label = String.format(String.format("%3s", '#'));
            }
            csv.addValue(label);
        }
        csv.newLine();

        for (int i = 0; i < map.length; i++) {
            // add labels left
            csv.addValue(String.format("%3d", i + 1));
            for (int j = 0; j < map[0].length; j++) {
                csv.addValue(map[i][j]);
            }
            csv.newLine();
        }

        return csv.save();
    }


    public Cell cellAt(char column, int row) {
        if (Character.isAlphabetic(column)) {
            int col;
            if (Character.isUpperCase(column)) {
                col = column - 'A';
            } else {
                col = (column - 'a') + 26;
            }

            row--; // the rows start from 1
            if (row < map.length && col < map[0].length) {
                return new Cell(col, row, map[row][col]);
            }
        }
        return null;
    }

    public void markPath(List<Cell> path, char overSpace, char overWater) {
        path.forEach(cell -> {
            switch (cell.getSign()) {
                case ' ':
                    updateSign(cell, overSpace);
                    break;
                case '~':
                    updateSign(cell, overWater);
                    break;
                default:
                    break;
            }
        });
    }

    public void updateSign(Cell cell, char sign) {
        map[cell.y][cell.x] = sign;
        cell.sign = sign;
    }

    public Cell north(Cell from) {
        if (from != null && from.y - 1 >= 0) {
            return new Cell(from.x, from.y - 1, map[from.y - 1][from.x]);
        } else {
            return null;
        }
    }

    public Cell east(Cell from) {
        if (from != null && from.x + 1 < map[0].length) {
            return new Cell(from.x + 1, from.y, map[from.y][from.x + 1]);
        } else {
            return null;
        }
    }

    public Cell south(Cell from) {
        if (from != null && from.y + 1 < map.length) {
            return new Cell(from.x, from.y + 1, map[from.y + 1][from.x]);
        } else {
            return null;
        }
    }

    public Cell west(Cell from) {
        if (from != null && from.x - 1 >= 0) {
            return new Cell(from.x - 1, from.y, map[from.y][from.x - 1]);
        } else {
            return null;
        }
    }

    public List<Cell> availableNeighbours(Cell from) {
        List<Cell> neighbours = new ArrayList<>(8);
        // north
        Cell neighbour = north(from);
        if (addIfAvailableNotNull(neighbours, neighbour)) {
            assignCost(neighbour, false);
        }

        //north-east
        neighbour = east(neighbour);
        if (addIfAvailableNotNull(neighbours, neighbour)) {
            assignCost(neighbour, true);
        }

        // east
        neighbour = east(from);
        if (addIfAvailableNotNull(neighbours, neighbour)) {
            assignCost(neighbour, false);

        }

        // south-east
        neighbour = south(neighbour);
        if (addIfAvailableNotNull(neighbours, neighbour)) {
            assignCost(neighbour, true);
        }

        // south
        neighbour = south(from);
        if (addIfAvailableNotNull(neighbours, neighbour)) {
            assignCost(neighbour, false);
        }

        // south-west
        neighbour = west(neighbour);
        if (addIfAvailableNotNull(neighbours, neighbour)) {
            assignCost(neighbour, true);
        }

        // west
        neighbour = west(from);
        if (addIfAvailableNotNull(neighbours, neighbour)) {
            assignCost(neighbour, false);
        }

        // north-west
        neighbour = north(neighbour);
        if (addIfAvailableNotNull(neighbours, neighbour)) {
            assignCost(neighbour, true);
        }
        return neighbours;
    }

    private void assignCost(Cell to, boolean diagonalMove) {
        switch (to.sign) {
            case ' ':
                to.cost = 1;
                break;
            case '~':
                to.cost = 2;
                break;
            default:
                to.cost = Double.MAX_VALUE;
        }
        if (diagonalMove) {
            to.cost += 0.5;
        }
    }

    private boolean addIfAvailableNotNull(List<Cell> to, Cell what) {
        if (what != null && to != null && what.sign != 'N') {
            to.add(what);
            return true;
        } else {
            return false;
        }
    }

    private void initMap() {
        String labels = rawMapList.get(0);
        labels = labels.replaceAll(",", "");
        int width = labels.length() - 1;
        int height = rawMapList.size() - 1;
        map = new char[height][width];

        //skip the first line with labels
        for (int i = 0; i + 1 < rawMapList.size(); i++) {
            //remove commas and numbers
            map[i] = rawMapList.get(i + 1).replaceAll("[0-9,]", "").toCharArray();
        }
    }

}
