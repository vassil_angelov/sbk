package com.vasko;

import java.io.FileWriter;
import java.io.IOException;

public class CsvFile {

    private static final String EXTENSION = ".csv";

    private String name;
    private StringBuilder content;

    public CsvFile(String name) {
        if (name != null) {
            this.name = name;
        }
        content = new StringBuilder();
    }

    public void addValue(Character value) {
        content.append(value);
        content.append(',');
    }

    public void addValue(String value) {
        content.append(value);
        content.append(',');
    }

    public void newLine() {
        content.deleteCharAt(content.length() - 1);
        content.append('\n');
    }

    public boolean save() {
        try {
            FileWriter fileWriter = new FileWriter(name + EXTENSION);
            fileWriter.append(content);
            fileWriter.flush();
            fileWriter.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

}
