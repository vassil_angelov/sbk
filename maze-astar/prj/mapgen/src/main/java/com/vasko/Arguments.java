package com.vasko;

import org.kohsuke.args4j.Option;

public class Arguments {

    @Option(name = "--width", aliases = "-w", usage = "Width (horizontal size) of the map", required = true)
    public int width;

    @Option(name = "--height", aliases = "-h", usage = "Height (vertical size) of the map", required = true)
    public int height;

    @Option(name = "--walls-count", aliases = "-wc", usage = "Number of walls to use in the map. Must comply with Walls+Water < width*height. Default is 12.5%(w*h-wtc)")
    public int wallsCount = -1;

    @Option(name = "--water-count", aliases = "-wtc", usage = "Number of water fields to use in the map. Must comply with Walls+Water < width*height. Default is 12.5%(w*h-wc)")
    public int waterCount = -1;

    @Option(name = "--file-name", aliases = "-f", usage = "Output filename. Default is \"test\"")
    public String fileName = "test";

    @Option(name = "--labels", aliases = "-l", usage = "Add labels to columns and rows")
    public boolean hasLabels = false;

    public void normalize() {

        if (width <= 0) {
            width = 5;
        }

        if (height <= 0) {
            height = 5;
        }

        int totalBlocks = height * width;

        if (wallsCount < 0 || wallsCount > Math.abs(totalBlocks - waterCount)) {

            wallsCount = (totalBlocks - (waterCount < 0 ? 0 : waterCount)) / 8;
        }

        if (waterCount < 0 || waterCount > Math.abs(totalBlocks - wallsCount)) {
            waterCount = (totalBlocks - (wallsCount < 0 ? 0 : wallsCount)) / 8;
        }
    }

    @Override
    public String toString() {
        return "Arguments{" +
                "width=" + width +
                ", height=" + height +
                ", wallsCount=" + wallsCount +
                ", waterCount=" + waterCount +
                ", fileName='" + fileName + '\'' +
                ", labels=" + hasLabels +
                '}';
    }
}
