package com.vasko;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import java.util.Iterator;

public class Main {

    public static void main(String[] args) {

        Arguments arguments = new Arguments();

        CmdLineParser parser = new CmdLineParser(arguments);
        try {
            parser.parseArgument(args);
            arguments.normalize();

            int totalBlocks = arguments.height * arguments.width;
            int whiteSpaceCount = totalBlocks - (arguments.waterCount + arguments.wallsCount);

            CharPool pool = new CharPool();
            pool.addChar(' ', whiteSpaceCount);
            pool.addChar('N', arguments.wallsCount);
            pool.addChar('~', arguments.waterCount);

            Iterator<Character> randomOrder = pool.randomOrder();
            CsvFile file = new CsvFile(arguments.fileName);

            if (arguments.hasLabels) {
                // add labels for the first 52 columns
                file.addValue(String.format("%3s", '#'));
                for (int i = 0; i < arguments.width && i < 52; i++) {

                    if (i < 26) {
                        file.addValue((char) ('A' + i));
                    } else {
                        file.addValue((char) ('a' + (i - 26)));
                    }

                }
                file.newLine();
            }
            for (int i = 0; i < arguments.height; i++) {
                if (arguments.hasLabels) {
                    // add labels left
                    file.addValue(String.format("%3d", i + 1));
                }
                for (int j = 0; j < arguments.width; j++) {
                    file.addValue(randomOrder.next());
                }
                file.newLine();
            }
            file.save();
            System.out.println("Success");
            System.out.println(arguments.toString());
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            parser.printSingleLineUsage(System.err);
            System.exit(-1);
        }
    }
}
