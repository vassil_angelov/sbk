package com.vasko;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class CharPool {

    private class Entry {
        char data;
        int occurrences;

        public Entry(char data, int occurrences) {
            this.data = data;
            this.occurrences = occurrences;
        }
    }

    private Random random;
    private StringBuffer buffer;
    private Iterator<Character> randomIterator = new Iterator<Character>() {
        @Override
        public boolean hasNext() {
            return buffer.length() > 0;
        }

        @Override
        public Character next() {
            try {
                return popRandom();
            } catch (IndexOutOfBoundsException e) {
                return null;
            }
        }
    };

    public CharPool() {
        random = new Random();
        buffer = new StringBuffer();
    }

    public void addChar(char ch, int occurrences) {
        for (int i = 0; i < occurrences; i++) {
            buffer.append(ch);
        }
    }

    public char popRandom() {
        int entriesSize = buffer.length();
        if (entriesSize > 0) {
            int position = random.nextInt(entriesSize);

            char ch = buffer.charAt(position);
            buffer.deleteCharAt(position);

            return ch;
        }
        throw new IndexOutOfBoundsException();
    }

    public Iterator<Character> randomOrder() {
        return randomIterator;
    }

}
