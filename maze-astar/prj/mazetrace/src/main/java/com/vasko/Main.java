package com.vasko;

public class Main {

    public static void main(String[] args) {
        if (args.length != 3) {
            System.out.println("Incorrect number of arguments. Needed 3, but got " + args.length);
            return;
        }

        Maze maze = new Maze();

        if (!maze.parse(args[0])) {
            System.out.println("Invalid file for maze - " + args[0]);
            return;
        }

        MazePath path = new MazePath(maze);

        if (!path.parse(args[1])) {
            System.out.println("Invalid file for path - " + args[1]);
            return;
        }

        maze.markPath(path.cellsList(), '*', '&');

        if (!maze.save(args[2])) {
            System.out.println("Unable to save new file for maze - " + args[2]);
            return;
        }
    }
}
