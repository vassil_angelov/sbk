package com.vasko;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MazePath {

    private List<Maze.Cell> path;
    private Maze maze;

    public MazePath(Maze maze) {
        path = new ArrayList<>();
        this.maze = maze;
    }

    public boolean parse(String file) {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {

            String firstLine;

            if ((firstLine = br.readLine()) != null && !firstLine.isEmpty()) {
                if (!firstLine.isEmpty()) {
                    //trim front
                    firstLine = firstLine.replaceAll("^\\s+", "");
                }
            }
            initPath(firstLine);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<Maze.Cell> cellsList() {
        return path;
    }

    private void initPath(String stringPath) {
        if (stringPath != null) {
            stringPath = stringPath.replaceAll("[\\[\\]]", "");

            String[] cellStrings = stringPath.split(", ");

            for (String cellString : cellStrings) {
//                System.out.println("Init path cell " + cellString.charAt(1) + " " + Integer.parseInt(cellString.substring(3, cellString.indexOf(')'))));
                Maze.Cell cell = maze.cellAt(cellString.charAt(1), Integer.parseInt(cellString.substring(3, cellString.indexOf(')'))));
                if (cell != null) {
                    path.add(cell);
                }
            }
        }
    }
}
